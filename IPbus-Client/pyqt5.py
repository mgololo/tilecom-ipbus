#!/usr/bin/env python

import sys

from PyQt5.QtWidgets import QApplication, QMainWindow, QMenu, QVBoxLayout, QSizePolicy, QMessageBox, QWidget, QLabel, QVBoxLayout, QPushButton
from PyQt5.QtGui import QIcon
from PyQt5 import QtWidgets, QtGui
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
plt.rcParams.update({'font.size': 24})
from PyQt5 import QtGui

import random

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.left = 40
        self.top = 40
        self.title = 'Ultra96 Temperature and voltage readings'
        self.width = 1405
        self.height = 1000
        self.iconName = "home.jpg"
        self.initUI()

    def initUI(self):
        self.setWindowIcon(QtGui.QIcon(self.iconName))
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        m = PlotCanvas(self, width=14, height=9.3)
        m.move(0,0)

        # Grid Layout
        grid = QtWidgets.QGridLayout()
        self.setLayout(grid)

        self.lbl1 = QtWidgets.QLabel(self)
        self.lbl1.setText('Monitoring data on TileCoM board')
        #self.lbl1.adjustSize()
        self.lbl1.resize(1000, 35)
        self.lbl1.setStyleSheet("color:green;"
                                       "font: bold 38pt 'Arial'")
        self.lbl1.move(250,10)

        self.lbl2 = QtWidgets.QLabel(self)
        self.lbl2.setPixmap(QtGui.QPixmap('ultra96.png'))
        #self.lbl2.adjustSize()
        self.lbl2.resize(130, 80)
        self.lbl2.move(0,10)

        self.lbl3 = QtWidgets.QLabel(self)
        self.lbl3.setPixmap(QtGui.QPixmap('temperature.png'))
        #self.lbl2.adjustSize()
        self.lbl3.resize(150, 100)
        self.lbl3.move(1300,10)

        #button = QPushButton('PyQt5 button', self)
        #button.setToolTip('This s an example button')
        #utton.move(260,0)
        #button.resize(140, 30)

        self.show()

class PlotCanvas(FigureCanvas):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        #self.axes = fig.add_subplot(222)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.plot()

    def plot(self):
        data = [random.random() for i in range(25)]
        #y = [1, 2, 3, 4, 5]
        y = np.loadtxt('PS_Temperature.txt', delimiter=',', unpack=True)
        x = [1, 2, 3, 4, 5, 6, 7, 8]
        ax = self.figure.add_subplot(221)
        ax.plot(x, y, 'r-')
        #ax.set_xlabel('Time (s)')
        ax.set_ylabel('Temp (Deg. Celcius)')
        ax.set_title('PS Temperature')

        y1 = np.loadtxt('Remote_Temperature.txt', delimiter=',', unpack=True)
        x1 = [1, 2, 3, 4, 5, 6, 7, 8]
        ax1 = self.figure.add_subplot(222)
        ax1.yaxis.tick_right()
        ax1.yaxis.set_label_position("right")
        ax1.plot(x1, y1, 'r-')
        #ax1.set_xlabel('Time (s)')
        ax1.set_ylabel('Temp (Deg. Celcius)')
        ax1.set_title('Remote Temperature')

        y2 = np.loadtxt('V_Int.txt', delimiter=',', unpack=True)
        x2 = [1, 2, 3, 4, 5, 6, 7, 8]
        ax2 = self.figure.add_subplot(223)
        ax2.plot(x2, y2, 'r-')
        ax2.set_xlabel('Time (s)')
        ax2.set_ylabel('Voltage')
        ax2.set_title('Init Voltage')

        y3 = np.loadtxt('V_Aux.txt', delimiter=',', unpack=True)
        x3 = [1, 2, 3, 4, 5, 6, 7, 8]
        ax3 = self.figure.add_subplot(224)
        ax3.yaxis.tick_right()
        ax3.yaxis.set_label_position("right")
        ax3.plot(x3, y3, 'r-')
        ax3.set_xlabel('Time (s)')
        ax3.set_ylabel('Voltage')
        ax3.set_title('Aux Voltage')

        self.draw()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
