from __future__ import print_function

from twisted.internet import reactor, protocol

import matplotlib.pyplot as plt
import numpy as np


class EchoClient(protocol.Protocol):
    """Once connected, send a message, then print the result."""

    x = [1, 2, 3, 4]
    y = [1, 2, 3, 4]

    def __init__(self):
        self.__buffer = ""
        self.frame_size = 13  # FRAME SIZE HER
        self.data = [0.0, 0.0, 0.0, 0.0]

    def connectionMade(self):
        print("connection made")
        #        self.transport.write(b"hello, world!")

        """    
        def dataReceived(self, data):
            "As soon as any data is received, write it back."
            #print("Temperature: %.1f", float(data))
            self.transport.loseConnection()
        """

    def dataReceived(self, data):
        """Protocol.dataReceived.
        Translates bytes into lines, and calls lineReceived (or
        rawDataReceived, depending on mode.)
        """
        print('data received called')
        self.__buffer = self.__buffer + data.decode('utf-8')
        print(len(self.__buffer))
        frame_size = self.frame_size
        while len(self.__buffer) >= frame_size:
            self.frame_received(self.__buffer[0:frame_size + 1])
            self.__buffer = self.__buffer[frame_size:]

    def frame_received(self, data):
        print(data)

    # plt.plot(self.x, self.data, label='Loaded from file!')
    # plt.show()

    def connectionLost(self, reason):
        pass
        #print("connection lost")


class EchoFactory(protocol.ClientFactory):
    protocol = EchoClient

    def clientConnectionFailed(self, connector, reason):
        print("Connection failed - goodbye!")
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print("Connection lost - goodbye!")
        reactor.stop()


# this connects the protocol to a server running on port 8000
def main():
    f = EchoFactory()
    reactor.connectTCP("192.168.1.39", 8000, f)
    reactor.run()


# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()